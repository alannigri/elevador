package com.company;

import java.util.Scanner;

public class IO {

    public static int andares(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe a qtde de andares do predio:");
        int andares = scanner.nextInt();
        return andares;
    }

    public static int capacidadeMax(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe a capacidade máxima do elevador:");
        int capMax = scanner.nextInt();
        return capMax;
    }

    public static void cheio(){
        System.out.println("Elevador cheio!");
    }

    public static void vazio(){
        System.out.println("Elevador vazio!");
    }

    public static void ultimoAndar(){
        System.out.println("Elevador já está no último andar!");
    }

    public static void terreo(){
        System.out.println("Elevador já está no terreo!");
    }

    public static int menu(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("MENU ELEVADOR");
        System.out.println("1 para entrar uma pessoa, 2 para sair uma pessoa");
        System.out.println("3 para subir andar, 4 para descer andar");
        System.out.println("Qualquer outra tecla para sair do programa");
        int opcao = scanner.nextInt();
        return opcao;
    }

    public static void situacaoAtual(int andar, int pessoas){
        System.out.println("O elevador está no andar " + andar + " com " + pessoas + " pessoas dentro");
    }
}
