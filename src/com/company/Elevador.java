package com.company;

public class Elevador extends Predio {
    private int andarAtual, capacidadeMaxima, quantidadeAtual;
    private static final int sobeAndar = +1, desceAndar = -1, entraPessoa = +1, saiPessoa = -1;

    public Elevador(int andares, int andarAtual, int capacidadeMaxima, int quantidadeAtual) {
        super(andares);
        this.andarAtual = andarAtual;
        this.capacidadeMaxima = capacidadeMaxima;
        this.quantidadeAtual = quantidadeAtual;
    }

    public void entra() {
        if (quantidadeAtual == capacidadeMaxima) {
            IO.cheio();
        } else {
            quantidadeAtual += entraPessoa;
        }
        IO.situacaoAtual(andarAtual, quantidadeAtual);
    }

    public void sai() {
        if (quantidadeAtual == 0) {
            IO.vazio();
        } else {
            quantidadeAtual += saiPessoa;
        }
        IO.situacaoAtual(andarAtual, quantidadeAtual);
    }

    public void sobe(){
        if(andarAtual == getAndares()){
            IO.ultimoAndar();
        }else{
            andarAtual += sobeAndar;
        }
        IO.situacaoAtual(andarAtual, quantidadeAtual);
    }

    public void desce(){
        if(andarAtual == 0){
            IO.terreo();
        }else{
            andarAtual += desceAndar;
        }
        IO.situacaoAtual(andarAtual, quantidadeAtual);
    }
}
