package com.company;

public class Main {

    public static void main(String[] args) {
        IO io = new IO();
        Elevador elevador = new Elevador(IO.andares(), 0, IO.capacidadeMax(), 0);
        int opcao;
        do{
            opcao = IO.menu();
            if (opcao == 1){
                elevador.entra();
            }else if (opcao == 2){
                elevador.sai();
            }else if(opcao == 3){
                elevador.sobe();
            }else if(opcao==4){
                elevador.desce();
            }else{
                opcao = 5;
            }
        }while (opcao != 5);
    }
}
