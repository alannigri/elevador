package com.company;

public class Predio {
    private int andares;

    public Predio() {
    }

    public Predio(int andares) {
        this.andares = andares;
    }

    public int getAndares() {
        return andares;
    }
}
